var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require("mongoose");
var endpoints = require("./endpoints");

var app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended : true }));
app.use(endpoints(express.Router()));

var mongoUrl = process.env.HAMMER_MONGODB_URL || "107.170.221.193";
var databaseName = process.env.HAMMER_DB_NAME || "hammer";
mongoose.connect("mongodb://" + mongoUrl + "/" + databaseName);

var port = process.env.PORT || 8080;
app.listen(port, function(){
	console.info("listening at ", port);
});
