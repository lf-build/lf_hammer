var mongoose = require("mongoose");
var Schema = mongoose.Schema;

module.exports = new Schema({
  "line1": {
      "type": "string",
      required : true
  },
  "line2": {
      "type": "string"
  },
  "city": {
      "type": "string",
      required : true
  },
  "state": {
      "type": "string",
      required : true
  },
  "zipCode": {
      "type": "string",
      required : true
  },
  "isPrimary": {
      "type": "boolean"
  }
});
