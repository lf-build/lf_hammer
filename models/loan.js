var mongoose = require("mongoose");
var BorrowerSchema = require("./borrower");
var FeeSchema = require("./fee");
var Schema = mongoose.Schema;


var LoanSchema = new Schema({
  "referenceNumber": {
      type: "string",
      required : true,
      unique : true
  },
  "customer": {
      type: "string",
      required :true
  },
  "fundingSource": {
      type: "string",
      required :true
  },
  "productType": {
      type: "string",
      required :true
  },
  "borrowers": [BorrowerSchema],
  "terms": {
    "loanAmount": {
        type: "number",
        format: "double"
    },
    "originationDate": {
        type: "date"
    },
    "firstPaymentDate": {
        type: "date"
    },
    "contractTermInMonths": {
        type: "number"
    },
    "paymentAmount": {
        type: "number",
        format: "double"
    },
    "rate": {
        type: "number",
        format: "double"
    },
    "rateType": {
        type: "string",
        enum: [
            "percent",
            "factor"
        ]
    },
    "paymentFrequency": {
        type: "string",
        enum: [
            "daily",
            "weekly",
            "biweekly",
            "monthly",
            "quarterly",
            "annually"
        ]
    },
    "gracePeriodInDays": {
        type: "number"
    },
    "fees": [FeeSchema],
    "bankInfo": {
      "routingNumber": {
          type: "string"
      },
      "accountNumber": {
          type: "string"
      },
      "accountType": {
          type: "string",
          "default": "checking",
          enum: [
              "checking",
              "savings"
          ]
      }
    }
  }
});

module.exports = mongoose.model("Loan", LoanSchema);
