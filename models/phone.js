var mongoose = require("mongoose");
var Schema = mongoose.Schema;

module.exports = new Schema({
  "type": {
      "type": "string",
      "default": "home",
      "enum": [
          "home",
          "work",
          "mobile",
          "other"
      ]
  },
  "number": {
      "type": "string",
      required : true
  },
  "isPrimary": {
      "type": "boolean"
  }
});
