var mongoose = require("mongoose");
var Schema = mongoose.Schema;

module.exports = new Schema({
  "name": {
      "type": "string",
      required : true
  },
  "type": {
      "type": "string",
      "default": "fixed",
      "enum": [
          "fixed",
          "percent"
      ]
  },
  "amount": {
      "type": "number",
      "format": "double",
      required : true
  }
});
