var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var AddressSchema = require("./address");
var PhoneSchema = require("./phone");

module.exports = new Schema({
  "firstName": {
      "type": "string",
      required : true
  },
  "middleInitial": {
      "type": "string"
  },
  "lastName": {
      "type": "string",
      required : true
  },
  "dateOfBirth": {
      "type": "date",
      "format": "date",
      required : true
  },
  "ssn": {
      "type": "string",
      required : true
  },
  "email": {
      "type": "string",
      "format": "email",
      required : true
  },
  "isPrimary": {
      "type": "boolean"
  },
  "referenceNumber" : {
      "type": "number",
      required: true
  },
  "addresses": [AddressSchema],
  "phones": [PhoneSchema]
});
