var mongoose = require("mongoose");
var FeeSchema = require("./fee");
var Schema = mongoose.Schema;

var LoanTransaction = new Schema({
    "loan": {
        type: "string",
        required: true
    },
    "referenceNumber": {
        "type": "string",
        required: true
    },
    "transactionNumber": {
        "type": "string",
        required: true
    },
    "reversalNumber": {
        "type": "string"
    },
    "transactionCode": {
        "type": Number,
        "enum": [
            100,
            101,
            120,
            121,
            122,
            123,
            124,
            125,
            126,
            127,
            150,
            151,
            180,
            181,
            200,
            201,
            202,
            203,
            204,
            205,
            206,
            207,
            208,
            209,
            220,
            221,
            222,
            223,
            250,
            251,
            280,
            281,
            306,
            307,
            312,
            313,
            400,
            401,
            440,
            441,
            444,
            445,
            448,
            449,
            452,
            453,
            454,
            455,
            460,
            461,
            462,
            463,
            464,
            465,
            486,
            487,
            626,
            627,
            628,
            629,
            724,
            725,
            726,
            727,
            940,
            941,
            944,
            945,
            948,
            949,
            952,
            953,
            988,
            989,
            1020,
            1021,
            1022,
            1023
        ],
        required: true
    },
    "description": {
        "type": "string"
    },
    "transactionDate": {
        "type": "date",
        required: true
    },
    "effectiveDate": {
        "type": "date"
    },
    "glDate": {
        "type": "date"
    },
    "amount": {
        "type": "number",
        "format": "double",
        required: true
    },
    "payment": {
        "number": {
            "type": "string",
            required: true
        },
        "method": {
            "type": "string",
            "enum": [
                "ach",
                "cc",
                "check",
                "cash"
            ],
            required: true
        },
        "amount": {
            "principal": {
                "type": "number",
                "format": "double",
                required: true
            },
            "interest": {
                "type": "number",
                "format": "double",
                required: true
            },
            "fees": [FeeSchema],
            "total": {
                "type": "number",
                "format": "double",
                required: true
            }
        }
    }
});

module.exports = mongoose.model("Transaction", LoanTransaction);