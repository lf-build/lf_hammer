[
    {
        'repeat:5': {
            loan : "1",
            referenceNumber : "{{guid()}}",
            transactionNumber : "{{integer(123423, 892342)}}",
            transactionCode : 204,
            effectiveDate : "{{moment(new Date())}}",
            amount : "{{floating(308.36, 308.36, 2)}}",
            payment : {
                "number" : "{{integer(10000, 5000)}}",
                method : "ach",
                amount : {
                    principal : 202.05,
                    interest : 106.31,
                    total : 308.36
                }
            }
            
        }
    }
]