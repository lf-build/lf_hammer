var Loan = require("./models/loan");
var Transaction = require("./models/transaction");
var Error = require("./models/error");

module.exports = function (router) {

    // finds the loan by the parameter "number"
    var findLoan = function (req, res, next) {
        if (!req.params.number) {
            res.status(400).json(new Error(400, "Invalid Loan reference number"));
        }

        Loan.findOne({ referenceNumber: req.params.number }, function (err, loan) {
            if (err)
                return res.status(500).json(new Error(500, ""));

            if (loan == null)
                return res.status(404).send();

            req.loan = loan.toObject(cleanOutput);
            console.log("loan found: ", loan.referenceNumber);
            next();
        });
    };

    var findTransactions = function(req, res, next){
        Transaction.find({ loan : req.params.number }, function(err, transactions){
            if (err)
                return res.status(500).json(new Error(500, ""));

            var cleanTransactions = [];
            transactions.forEach(function(e,i){
                cleanTransactions.push(e.toObject(cleanOutput));
            });

            req.transactions = cleanTransactions;
            next();
        });
    };
    
    var findBorrower = function(req, res, next) {
        if (!req.params.number) {
            res.status(400).json(new Error(400, "Invalid Loan reference number"));
        }
        Loan.findOne({ referenceNumber : req.params.number,
                       "borrowers.referenceNumber": req.params.reference },
        function(err, loan){
            if (err) {
                return res.status(500).json(new Error(500, ""));
            }
           
            if (loan == null) {
                return res.status(404).send();
            }
            
            for (var i = 0; i < loan.borrowers.length; i++) {
                if (loan.borrowers[i].referenceNumber == req.params.reference) {
                    req.borrower = loan.borrowers[i].toObject(cleanOutput);
                    continue;
                }
            }
            
            // req.borrower = loan.borrowers[0].toObject(cleanOutput);
            
            console.log("borrower found: ", req.params.reference, "for loan: ", loan.referenceNumber);
            next();
        });
    }

    // used to remove id and version properties from mongo docs
    var cleanOutput = {
        transform: function (d, r) {
            delete r._id;
            delete r.__v;
            return r;
        }
    };

    router.post("/onboard", [
        // validates input
        function (req, res, next) {
            if (Object.keys(req.body).length == 0)
                res.status(400).json(new Error(400, "Invalid input"));
            next();
        },
        function (req, res) {

            // if it is a single object, convert to array
            if (!Array.isArray(req.body))
                req.body = [req.body];

            var loans = [];
            for (var i in req.body) {
                loans.push(new Loan(req.body[i]));
            }

            Loan.create(loans, function (err) {
                if (err) {

                    // invalid schema
                    if (err.name === 'ValidationError')
                        res.status(400).json(new Error(400, "Invalid input"));

                    // duplicate loan
                    if(err.name === "MongoError" && err.code == 11000){

                        // gets the loan number from error message
                        var n = /(\")(.+)(?=\")/.exec(err.err)[2];
                        console.log(n);
                        res.status(400).json(new Error(400, "The loan with reference number " + n + " has been already onboard"));
                    }

                    res.status(500).json(new Error(500, "We could not process your request"));
                }
                res.status(200).send();
            });
        }]);
        
    router.get("/:number", [findLoan, findTransactions, function (req, res) {
        res.json({
            info: req.loan,
            transactions: req.transactions
        });
    }]);

    router.get("/:number/info", [findLoan, function (req, res) {
        res.json(req.loan);
    }]);

    router.get("/:number/transactions", [findLoan, findTransactions, function (req, res) {
        res.json(req.transactions);
    }]);
    
    router.get("/:number/borrowers", [findLoan, function (req, res) {
        res.json(req.loan.borrowers);
    }]);
    
    router.get("/:number/borrowers/:reference", [findBorrower, function (req, res) {
        res.json(req.borrower);
    }]);
    
    router.post("/:number/borrowers/:reference", [
        // validates inputs
        function (req, res, next) {
            if (Object.keys(req.body).length == 0)
                res.status(400).json(new Error(400, "Invalid input"));
            next();
        },
        function (req, res, next) {
            if (!req.params.number) {
                res.status(400).json(new Error(400, "Invalid Loan reference number"));
            }
            if (!req.params.reference) {
                res.status(400).json(new Error(400, "Invalid Borrower reference number"));
            }
            
            Loan.findOne({ referenceNumber : req.params.number,
                           "borrowers.referenceNumber": req.params.reference },
            function(err, loan){
                if (err) {
                    return res.status(500).json(new Error(500, ""));
                }
            
                if (loan == null) {
                    return res.status(404).send();
                }
                
                var borrower;
                
                for (var i = 0; i < loan.borrowers.length; i++) {
                    if (loan.borrowers[i].referenceNumber == req.params.reference) {
                        borrower = loan.borrowers[i];
                    }
                }

                for (var update in req.body) {
                    borrower[update] = req.body[update];
                }
                
                loan.save(function() {
                    if (!err) { console.log("Success!") }
                })
                res.json(borrower.toObject(cleanOutput));
            });
        }
    ]);
    return router;
};
