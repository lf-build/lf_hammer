FROM node:latest

RUN npm cache clear -f \
	&& npm install -g n \
	&& npm install -g npm \
	&& npm cache clear -f

ADD . /app
WORKDIR /app

RUN npm install

EXPOSE 8080

CMD ["node", "server.js"]
